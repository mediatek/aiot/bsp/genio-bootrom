#!/usr/bin/env python3
# SPDX-License-Identifier: MIT
# Copyright 2021 BayLibre, SAS.
# Copyright 2023 MediaTek Inc.

import setuptools

setuptools.setup(
    name="genio-bootrom",
    use_scm_version={
        'write_to': 'version.py',
        'local_scheme':'no-local-version'
    },
    setup_requires = ['setuptools_scm'],
    author="Fabien Parent",
    maintainer="Pablo Sun",
    maintainer_email="pablo.sun@mediatek.com",
    description="Tool to bootstrap the flashing on MediaTek Genio SoCs",
    url="https://gitlab.com/mediatek/aiot/bsp/genio-bootrom",
    packages=setuptools.find_packages(),
    include_package_data=True,
    package_data={
        "": ["bin/bootrom-tool", "bin/bootrom-tool.exe",
             "bin/amd64/linux/bootrom-tool", "bin/amd64/windows/bootrom-tool.exe",
             "bin/x86_64/linux/bootrom-tool", "bin/x86_64/windows/bootrom-tool.exe",
             "bin/aarch64/linux/bootrom-tool"
            ],
    },
    entry_points={
        'console_scripts': [
            'aiot-bootrom=aiot_bootrom.bootrom:main',
            'genio-bootrom=aiot_bootrom.bootrom:main',
        ]},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: Other/Proprietary License",
        "Operating System :: POSIX :: Linux",
        "Operating System :: Microsoft :: Windows",
        "Topic :: Software Development :: Embedded Systems",
    ],
);
